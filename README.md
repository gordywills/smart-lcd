# HD44780 LCD driver interface for RPi

## Introduction
When I came to use a a 16x2 LCD display module with my RPi, I could find plenty of python libraries, but no PHP ones, so I decided to write my own. What you see here is a basic usage model. I have exposed two public functions in the primary Worker class that allow you to send a DateTime object to display the date and time or just two strings of arbitrary text.  I have not implemented any read from the HD44780 or the custom character write functions.

![16x2 LCD Display](http://skpang.co.uk/catalog/images/lcd/lcd162b-yhy.jpg "16x2 LCD Display with HD44780 driver")

## Resources
In preparing the code I use several resources to learn the behaviour of the HD44780 driver.  In the LCD\Resources folder of the source you will find the driver data sheet plus some tutorials.  You will also find [urls.md](LCD/Resources/urls.md) which has some online links as well.

## Installing
Using composer is preferred:

Commandline: `php composer.phar require "gordywills/smart-lcd":"dev-master"`

composer.json:

```json
{
    "require": {
        "gordywills/smart-lcd": "dev-master"
    }
}
```

## Use
**Note for the GPIO to operate correctly this script must be run as `root` or using `sudo`** 

Instantiate a new instance using the factory:

```php
<?php
use LCD\WorkerFactory;
$LCD = WorkerFactory::create();
```

Then display strings using the public methods:

```php
<?php
$firstLine = 'This is a 16x2';
$secondLine = 'LCD Display';
$LCD->displayMessages($firstLine,$secondLine);
```

### Public Methods
* `public function displayTime(\DateTime $dateTime)` display the date and time in the date time object
* `public function displayMessages($line1, $line2)` display the text in lines 1 and 2 (max 40 chars per line)
* `public function displayDoubleHeightTime(\DateTime $dateTime)` display the time using double height characters (relies on using the default programmable characters)

## Example
```php
#!/usr/bin/env php
<?php

//initiate the composer autoload
require 'vendor/autoload.php';

//Dependencies
use LCD\WorkerFactory;

$LCD = WorkerFactory::create();
$firstLine = 'This is a 16x2';
$secondLine = 'LCD Display';
$firstLongLine = 'This message is very long.';
$secondLongLine = 'It causes the display to scroll.';
while (true) {
    $LCD->displayTime(new DateTime());
    sleep(15);
    $LCD->displayMessages($firstLine,$secondLine);
    sleep (15);
    $LCD->displayTime(new DateTime());
    sleep(15);
    $LCD->displayMessages($firstLongLine,$secondLongLine);
    sleep (15);
}
```

## Pin outs
The default pins are:
```php
<?php
$pins = [
    'D7'             => 22,
    'D6'             => 21,
    'D5'             => 17,
    'D4'             => 23,
    'D3'             => 20,
    'D2'             => 13,
    'D1'             => 19,
    'D0'             => 26,
    'RegisterSelect' => 25,
    'EnablePin'      => 24,
];
```
If you want to use different pins then copy `pins.config.php.dist` to a location of you choice as `pins.config.php` and pass to the WorkerFactory as:

```php
<?php
use LCD\WorkerFactory;
$LCD = WorkerFactory::create('pins.config.php');
```
## Custom Characters
It is possible to program 8 custom characters.  By default this library establishes them as glyphs that can be used to display double height numbers in the display and uses these for the publich function `public function displayDoubleHeightTime(\DateTime $dateTime)`.

If you wish to create your own custom characters then copy `programmableCharacters.config.php.dist` to a location of your choice as `programmableCharacters.config.php` and edit the contents such that a 0 is off and 1 is on with in each of eight lines per eight characters.  (Note that the first 3 digits are always 0 and the last 5 are the ones that draw the pixels on each line.  See Fig.5. in [Julyan llett - "How to use intelligent L.C.D.s - Part One"](http://lcd-linux.sourceforge.net/pdfdocs/lcd1.pdf) for more detail).
```php
<?php
use LCD\WorkerFactory;
$LCD = WorkerFactory::create('pins.config.php', 'programmableCharacters.config.php');
``` 
To display your custom characters then add `<<1>>` to your display string to show character 1. eg `$message = "This is custom Char 7: <<7>>";`
## Extending
As I did not need it I have not implemented read busy signal function.  Should you need it then they should eb relatively simple to add to `LCD\Instruction\Command` and `LCD\Instruction\Content`.
