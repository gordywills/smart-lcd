<?php

namespace LCD;

use LCD\Instruction\CharacterCodes;
use LCD\Instruction\Command;
use LCD\Instruction\Content;
use LCD\Instruction\Instruction;

/**
 * Class LCD\Worker
 *
 * The main class for interacting with the LCD display.  from here you can display the time from a dateTime object or
 * send any arbitrary two lines of character data to the LCD.
 *
 * @package LCD
 */
class Worker
{
    /**
     * The default scroll speed in delay between single character scroll steps
     *
     * @var int
     */
    const SCROLL_SPEED = 1000000;
    /**
     * The default reset back to beginning delay for scroll once it has completed
     *
     * @var int
     */
    const SCROLL_RESET_DELAY = 2000000;
    /**
     * @var Command
     */
    private $command;
    /**
     * @var Content
     */
    private $content;

    /**
     * LCD constructor.
     * Corrals the the helpers from the driver and initialises and clears the display
     *
     * @param Command $command A representation of the command interface
     * @param Content $content A representation of the DDRAM interface
     */
    public function __construct(Command $command, Content $content)
    {
        $this->command = $command;
        $this->content = $content;
        $this->initialiseLCDDisplay();
        $this->command->initialiseProgrammableChars();
        $this->command->clearDisplay();
    }

    /**
     * When powering on the LCD its state is not deterministic.  This method ensure that the driver is expecting 8 bit
     * input is clear and the cursor is in the top left position.
     */
    private function initialiseLCDDisplay()
    {
        $this->command->functionSet(Command::EIGHT_BIT_DATA_LENGTH, Command::TWO_LINE_DISPLAY,
            Command::FIVE_BY_EIGHT_FONT);
        $this->command->displayOnOff(Command::DISPLAY_ON, Command::CURSOR_OFF, Command::BLINK_OFF);
        $this->command->entryModeSet(Command::CURSOR_INCREMENT, Command::ENTRY_MODE_NO_SHIFT);
    }

    /**
     * Given a DateTime object render the time and date to the LCD screen.
     *
     * @param \DateTime $dateTime A valid DateTime Object
     */
    public function displayTime(\DateTime $dateTime)
    {
        $string1stLine = str_pad($dateTime->format('H:i D d'), Instruction::WINDOW_WIDTH, ' ', STR_PAD_BOTH);
        $string2ndLine = str_pad($dateTime->format('F Y'), Instruction::WINDOW_WIDTH, ' ', STR_PAD_BOTH);
        $this->displayMessages($string1stLine, $string2ndLine);
    }

    /**
     * Given a DateTime object render the time in double height to the LCD screen.  This assume the default
     * programmable chars
     *
     * @param \DateTime $dateTime A valid DateTime Object
     */
    public function displayDoubleHeightTime(\DateTime $dateTime)
    {
        $timeString = $dateTime->format('H:i');
        $timeChars = str_split($timeString);
        $timeLineOne = '';
        $timeLineTwo = '';
        foreach ($timeChars as $char) {
            $timeLineOne .= CharacterCodes::DOUBLE_HEIGHT_NUMBER_LOOKUP[$char]['line1'];
            $timeLineTwo .= CharacterCodes::DOUBLE_HEIGHT_NUMBER_LOOKUP[$char]['line2'];
        }

        $string1stLine = str_pad('', ceil((Instruction::WINDOW_WIDTH - $this->content->countStringLength($timeLineOne))/2), ' ')
                         .$timeLineOne
                         .str_pad('', floor((Instruction::WINDOW_WIDTH - $this->content->countStringLength($timeLineOne))/2), ' ');
        $string2ndLine = str_pad('', ceil((Instruction::WINDOW_WIDTH - $this->content->countStringLength($timeLineTwo))/2), ' ')
                         .$timeLineTwo
                         .str_pad('', floor((Instruction::WINDOW_WIDTH - $this->content->countStringLength($timeLineTwo))/2), ' ');
        $this->displayMessages($string1stLine, $string2ndLine);
    }

    /**
     * Given two lines of text display them both in the LCD display.  If either line is over 16 chars then a scroll will
     * be triggered
     *
     * @param string $line1 the first line of text max 40 chars
     * @param string $line2 the second line of text max 40 chars
     */
    public function displayMessages($line1, $line2)
    {
        $this->command->clearDisplay();
        $this->displayFirstLine($line1);
        $this->displaySecondLine($line2);
        $this->scrollDisplay(max($this->content->countStringLength($line1), $this->content->countStringLength($line2)),
            2);
    }

    /**
     * A helper that prints just the first line
     *
     * @param string $line text to be shown on the first line max 40 chars
     */
    private function displayFirstLine($line)
    {
        $this->command->cursorHome();
        $this->content->printContentAtCursor($line);
    }

    /**
     * A helper that prints just the second line
     *
     * @param string $line text to be shown on the second line max 40 chars
     */
    private function displaySecondLine($line)
    {
        $this->command->scrollToAddress(Command::START_OF_SECOND_LINE);
        $this->content->printContentAtCursor($line);
    }

    /**
     * scroll the display such that a sting longer than the viewing window (16 chars) will be scrolled into view
     *
     * @param int $stringLength     The length of the string to be viewed this ensures that un-required white space at
     *                              the end of the DDRAM is not scrolled into view
     * @param int $repetitions      The number of times to repeat the scrolling
     * @param int $scrollSpeed      The scroll speed in delay between single character scroll steps
     * @param int $scrollResetDelay The default reset back to beginning delay for scroll once it has completed
     */
    private function scrollDisplay(
        $stringLength = Instruction::DISPLAY_WIDTH,
        $repetitions = 2,
        $scrollSpeed = self::SCROLL_SPEED,
        $scrollResetDelay = self::SCROLL_RESET_DELAY
    ) {
        if ($stringLength <= Instruction::WINDOW_WIDTH) {
            return;
        }
        $steps = $stringLength - Instruction::WINDOW_WIDTH;
        for ($i = 1; $i <= $repetitions; $i++) {
            for ($step = 1; $step <= $steps; $step++) {
                usleep($scrollSpeed);
                $this->command->cursorDisplayShift(Command::SHIFT_DISPLAY, Command::SHIFT_LEFT);
            }
            usleep($scrollResetDelay);
            $this->command->cursorHome();
        }
    }

}

