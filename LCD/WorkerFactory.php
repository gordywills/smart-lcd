<?php

namespace LCD;

use LCD\Driver\Connection;
use LCD\Driver\GPIOPins;
use LCD\Instruction\CharacterCodes;
use LCD\Instruction\Command;
use LCD\Instruction\Content;
use PhpGpio\Gpio;

/**
 * Class LCD\WorkerFactory
 * A class to create a well formed LCD worker
 *
 * @package LCD
 */
class WorkerFactory
{

    /**
     * Private constructor to prevent instantiation
     */
    private function __construct()
    {
    }

    /**
     * The factory method.
     * Receive or create all dependencies for the LCD\Worker class and then inject them into the object.
     *
     * @param string|null     $pinConfigFile               A file containing a valid representation of the active pins
     *                                                     (see pins.config.php.dist for more)
     * @param string|null     $programmableCharsConfigFile A file containing a representation of the the programmable
     *                                                     characters (see programmableChars.config.dist for more)
     * @param Gpio|null       $gpio                        A GPIO control object
     * @param Connection|null $driver                      A representation of the Instruction for utility commands.
     * @param Command|null    $command                     A representation of the command interface
     * @param Content|null    $content                     A representation of the DDRAM interface
     *
     * @return Worker
     */
    public static function create(
        $pinConfigFile = null,
        $programmableCharsConfigFile = null,
        Gpio $gpio = null,
        Connection $driver = null,
        Command $command = null,
        Content $content = null
    ) {
        $pins = new GPIOPins($pinConfigFile);
        $programmableChars = new CharacterCodes($programmableCharsConfigFile);
        if (is_null($gpio)) {
            $gpio = new Gpio();
        }
        if (is_null($driver)) {
            $driver = new Connection($gpio, $pins);
        }
        if (is_null($command)) {
            $command = new Command($driver, $programmableChars);
        }
        if (is_null($content)) {
            $content = new Content($driver, $programmableChars);
        }

        return new Worker($command, $content);
    }
}