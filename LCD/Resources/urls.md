Some useful URLs

[HD44780U (LCD-II)(Dot Matrix Liquid Crystal Display Controller/Driver)](https://www.sparkfun.com/datasheets/LCD/HD44780.pdf)

[Hitachi HD44780 LCD controller Wiki](https://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller)

[HD44780 LCD Starter Guide](https://web.stanford.edu/class/ee281/handouts/lcd_tutorial.pdf)

[Julyan llett - "How to use intelligent L.C.D.s - Part One"](http://lcd-linux.sourceforge.net/pdfdocs/lcd1.pdf)

[Julyan llett - "How to use intelligent L.C.D.s - Part Two"](http://lcd-linux.sourceforge.net/pdfdocs/lcd2.pdf)

[Double Height Digits on a Character LCD Module](https://arduinoplusplus.wordpress.com/2016/07/04/double-height-digits-on-a-character-lcd-module/)