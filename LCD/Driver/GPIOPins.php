<?php

namespace LCD\Driver;

/**
 * Class LCD\Driver\GPIOPins
 * A config class that holds the pin outs of the GPIO attached to the LCD driver pins
 *
 * @package LCD
 */
class GPIOPins
{
    /**
     * @var array
     */
    private $registerPins;
    /**
     * @var int
     */
    private $enablePin;
    /**
     * @var int
     */
    private $registerSelectPin;

    /**
     * Load the pins config file if it exists, or revert to default
     *
     * @param string $file path and file name of the pins config file (see pins.config.php.dist for more)
     */
    public function __construct($file = null)
    {
        if (!is_null($file) && is_file($file)) {
            $pins = require $file;
        } else {
            $pins = [
                'D7'             => 22,
                'D6'             => 21,
                'D5'             => 17,
                'D4'             => 23,
                'D3'             => 20,
                'D2'             => 13,
                'D1'             => 19,
                'D0'             => 26,
                'RegisterSelect' => 25,
                'EnablePin'      => 24,
            ];
        }
        $this->registerPins[7] = $pins['D7'];
        $this->registerPins[6] = $pins['D6'];
        $this->registerPins[5] = $pins['D5'];
        $this->registerPins[4] = $pins['D4'];
        $this->registerPins[3] = $pins['D3'];
        $this->registerPins[2] = $pins['D2'];
        $this->registerPins[1] = $pins['D1'];
        $this->registerPins[0] = $pins['D0'];
        $this->registerSelectPin = $pins['RegisterSelect'];
        $this->enablePin = $pins['EnablePin'];

    }

    /**
     * Return the pin number for the specified bit digit
     *
     * @param int $digit the bit digit to find the pin number for
     *
     * @return int
     */
    public function getRegisterPin($digit)
    {
        return $this->registerPins[$digit];
    }

    /**
     * return the pin number of the register select pin
     *
     * @return int
     */
    public function getRegisterSelectPin()
    {
        return $this->registerSelectPin;
    }

    /**
     * return the pin number of the enable pin
     *
     * @return int
     */
    public function getEnablePin()
    {
        return $this->enablePin;
    }

}