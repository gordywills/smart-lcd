<?php

namespace LCD\Driver;

use PhpGpio\Gpio;

/**
 * Class LCD\Driver\Connection
 * A class to control the physical connection between GPIO and LCD driver
 *
 * @package LCD
 */
class Connection
{
    /**#@+
     * which LCD register to use
     *
     * @var int
     */
    const DATA_REGISTER    = 1;
    const COMMAND_REGISTER = 0;

    /**
     * A delay before the first instruction is sent to ensure tha LCD driver has fully powered up.
     */
    const INITIALISATION_DELAY = 20000;

    /**
     * A delay between executions to allow the LCD Instruction to complete its functionality before the next
     * instruction is sent.  It is set at 2.0ms as the longest duration instruction is 1.52ms according to the data
     * sheet (LCD\Resources\HD44780.pdf).
     */
    const INSTRUCTION_EXECUTION_DELAY = 20000;

    /**
     * @var Gpio
     */
    private $gpio;

    /**
     * @var GPIOPins
     */
    private $gpioPins;

    /**
     * set up the GPIO pins and clear out any half pending instructions or power on glitches from the LCD driver
     *
     * @param Gpio     $gpio An object to represent the GPIO interface
     * @param GPIOPins $pins A config object detailing the connections between GPIO pins and LCD driver pins
     */
    public function __construct(Gpio $gpio, GPIOPins $pins)
    {
        exec('sudo modprobe w1-gpio');
        $this->gpio = $gpio;
        $this->gpioPins = $pins;
        $this->setUpPins();
        $this->powerOnInitialiseClearPreviousSettings();
    }

    /**
     * Set up the GPIO pins in use and set them all to off.
     */
    private function setUpPins()
    {
        $this->gpio->setup($this->gpioPins->getEnablePin(), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterSelectPin(), Gpio::DIRECTION_OUT);

        $this->gpio->output($this->gpioPins->getEnablePin(), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterSelectPin(), Gpio::IO_VALUE_OFF);

        $this->gpio->setup($this->gpioPins->getRegisterPin(0), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(1), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(2), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(3), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(4), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(5), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(6), Gpio::DIRECTION_OUT);
        $this->gpio->setup($this->gpioPins->getRegisterPin(7), Gpio::DIRECTION_OUT);

        $this->gpio->output($this->gpioPins->getRegisterPin(0), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(1), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(2), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(3), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(4), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(5), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(6), Gpio::IO_VALUE_OFF);
        $this->gpio->output($this->gpioPins->getRegisterPin(7), Gpio::IO_VALUE_OFF);

    }

    /**
     * When powering on the LCD its state is not deterministic. This method sets the LCD driver ready to receive a
     * command telling what width the input is.
     */
    private function powerOnInitialiseClearPreviousSettings()
    {
        usleep(self::INITIALISATION_DELAY);
        $pattern = [0, 0, 1, 1, 0, 0, 0, 0];
        for ($i = 1; $i <= 3; $i++) {
            $this->sendInstruction($pattern, self::COMMAND_REGISTER);
        }
    }

    /**
     * Build and instruction on the LCD driver register and then call transact
     *
     * @param array $pattern  An 8 bit array of the instruction to be sent in human readable binary order
     * @param int   $register which register (DDRAM or Command) use class constants
     */
    public function sendInstruction($pattern, $register)
    {
        $this->gpio->output($this->gpioPins->getRegisterSelectPin(), $register);
        $reversedPattern = array_reverse($pattern);// so that indexes represent bit position

        foreach ($reversedPattern as $bit => $value) {
            $this->gpio->output($this->gpioPins->getRegisterPin($bit), $value);
        }

        $this->transact();
    }

    /**
     * Once an instruction has be set on the register, toggle enable pin to make the LCD driver read it
     */
    private function transact()
    {
        $this->gpio->output($this->gpioPins->getEnablePin(), Gpio::IO_VALUE_ON);
        usleep(self::INSTRUCTION_EXECUTION_DELAY);
        $this->gpio->output($this->gpioPins->getEnablePin(), Gpio::IO_VALUE_OFF);
        usleep(self::INSTRUCTION_EXECUTION_DELAY);
    }
}