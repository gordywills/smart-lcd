<?php

namespace LCD\Instruction;

/**
 * Class LCD\Instruction\CharacterCodes
 * A utility class that hold the character patterns
 *
 * @package LCD
 */
class CharacterCodes
{
    /**
     * The character patterns for the character in the array key. These are available in the definition of the
     * character map in the LCD driver datasheet (LCD\Resources\HD44780.pdf).
     */
    const CHARACTERS = [
        ' '     => [0, 0, 1, 0, 0, 0, 0, 0],
        '!'     => [0, 0, 1, 0, 0, 0, 0, 1],
        '"'     => [0, 0, 1, 0, 0, 0, 1, 0],
        '#'     => [0, 0, 1, 0, 0, 0, 1, 1],
        '$'     => [0, 0, 1, 0, 0, 1, 0, 0],
        '%'     => [0, 0, 1, 0, 0, 1, 0, 1],
        '&'     => [0, 0, 1, 0, 0, 1, 1, 0],
        '('     => [0, 0, 1, 0, 1, 0, 0, 0],
        ')'     => [0, 0, 1, 0, 1, 0, 0, 1],
        '*'     => [0, 0, 1, 0, 1, 0, 1, 0],
        '+'     => [0, 0, 1, 0, 1, 0, 1, 1],
        ','     => [0, 0, 1, 0, 1, 1, 0, 0],
        '-'     => [0, 0, 1, 0, 1, 1, 0, 1],
        '.'     => [0, 0, 1, 0, 1, 1, 1, 0],
        '/'     => [0, 0, 1, 0, 1, 1, 1, 1],
        '0'     => [0, 0, 1, 1, 0, 0, 0, 0],
        '1'     => [0, 0, 1, 1, 0, 0, 0, 1],
        '2'     => [0, 0, 1, 1, 0, 0, 1, 0],
        '3'     => [0, 0, 1, 1, 0, 0, 1, 1],
        '4'     => [0, 0, 1, 1, 0, 1, 0, 0],
        '5'     => [0, 0, 1, 1, 0, 1, 0, 1],
        '6'     => [0, 0, 1, 1, 0, 1, 1, 0],
        '7'     => [0, 0, 1, 1, 0, 1, 1, 1],
        '8'     => [0, 0, 1, 1, 1, 0, 0, 0],
        '9'     => [0, 0, 1, 1, 1, 0, 0, 1],
        ':'     => [0, 0, 1, 1, 1, 0, 1, 0],
        ';'     => [0, 0, 1, 1, 1, 0, 1, 1],
        '<'     => [0, 0, 1, 1, 1, 1, 0, 0],
        '='     => [0, 0, 1, 1, 1, 1, 0, 1],
        '>'     => [0, 0, 1, 1, 1, 1, 1, 0],
        '?'     => [0, 0, 1, 1, 1, 1, 1, 1],
        '@'     => [0, 1, 0, 0, 0, 0, 0, 0],
        'A'     => [0, 1, 0, 0, 0, 0, 0, 1],
        'B'     => [0, 1, 0, 0, 0, 0, 1, 0],
        'C'     => [0, 1, 0, 0, 0, 0, 1, 1],
        'D'     => [0, 1, 0, 0, 0, 1, 0, 0],
        'E'     => [0, 1, 0, 0, 0, 1, 0, 1],
        'F'     => [0, 1, 0, 0, 0, 1, 1, 0],
        'G'     => [0, 1, 0, 0, 0, 1, 1, 1],
        'H'     => [0, 1, 0, 0, 1, 0, 0, 0],
        'I'     => [0, 1, 0, 0, 1, 0, 0, 1],
        'J'     => [0, 1, 0, 0, 1, 0, 1, 0],
        'K'     => [0, 1, 0, 0, 1, 0, 1, 1],
        'L'     => [0, 1, 0, 0, 1, 1, 0, 0],
        'M'     => [0, 1, 0, 0, 1, 1, 0, 1],
        'N'     => [0, 1, 0, 0, 1, 1, 1, 0],
        'O'     => [0, 1, 0, 0, 1, 1, 1, 1],
        'P'     => [0, 1, 0, 1, 0, 0, 0, 0],
        'Q'     => [0, 1, 0, 1, 0, 0, 0, 1],
        'R'     => [0, 1, 0, 1, 0, 0, 1, 0],
        'S'     => [0, 1, 0, 1, 0, 0, 1, 1],
        'T'     => [0, 1, 0, 1, 0, 1, 0, 0],
        'U'     => [0, 1, 0, 1, 0, 1, 0, 1],
        'V'     => [0, 1, 0, 1, 0, 1, 1, 0],
        'W'     => [0, 1, 0, 1, 0, 1, 1, 1],
        'X'     => [0, 1, 0, 1, 1, 0, 0, 0],
        'Y'     => [0, 1, 0, 1, 1, 0, 0, 1],
        'Z'     => [0, 1, 0, 1, 1, 0, 1, 0],
        '['     => [0, 1, 0, 1, 1, 0, 1, 1],
        ']'     => [0, 1, 0, 1, 1, 1, 0, 1],
        '^'     => [0, 1, 0, 1, 1, 1, 1, 0],
        '_'     => [0, 1, 0, 1, 1, 1, 1, 1],
        'a'     => [0, 1, 1, 0, 0, 0, 0, 1],
        'b'     => [0, 1, 1, 0, 0, 0, 1, 0],
        'c'     => [0, 1, 1, 0, 0, 0, 1, 1],
        'd'     => [0, 1, 1, 0, 0, 1, 0, 0],
        'e'     => [0, 1, 1, 0, 0, 1, 0, 1],
        'f'     => [0, 1, 1, 0, 0, 1, 1, 0],
        'g'     => [0, 1, 1, 0, 0, 1, 1, 1],
        'h'     => [0, 1, 1, 0, 1, 0, 0, 0],
        'i'     => [0, 1, 1, 0, 1, 0, 0, 1],
        'j'     => [0, 1, 1, 0, 1, 0, 1, 0],
        'k'     => [0, 1, 1, 0, 1, 0, 1, 1],
        'l'     => [0, 1, 1, 0, 1, 1, 0, 0],
        'm'     => [0, 1, 1, 0, 1, 1, 0, 1],
        'n'     => [0, 1, 1, 0, 1, 1, 1, 0],
        'o'     => [0, 1, 1, 0, 1, 1, 1, 1],
        'p'     => [0, 1, 1, 1, 0, 0, 0, 0],
        'q'     => [0, 1, 1, 1, 0, 0, 0, 1],
        'r'     => [0, 1, 1, 1, 0, 0, 1, 0],
        's'     => [0, 1, 1, 1, 0, 0, 1, 1],
        't'     => [0, 1, 1, 1, 0, 1, 0, 0],
        'u'     => [0, 1, 1, 1, 0, 1, 0, 1],
        'v'     => [0, 1, 1, 1, 0, 1, 1, 0],
        'w'     => [0, 1, 1, 1, 0, 1, 1, 1],
        'x'     => [0, 1, 1, 1, 1, 0, 0, 0],
        'y'     => [0, 1, 1, 1, 1, 0, 0, 1],
        'z'     => [0, 1, 1, 1, 1, 0, 1, 0],
        '{'     => [0, 1, 1, 1, 1, 0, 1, 1],
        '|'     => [0, 1, 1, 1, 1, 1, 0, 0],
        '}'     => [0, 1, 1, 1, 1, 1, 0, 1],
        '<<0>>' => [0, 0, 0, 0, 0, 0, 0, 0],
        '<<1>>' => [0, 0, 0, 0, 0, 0, 0, 1],
        '<<2>>' => [0, 0, 0, 0, 0, 0, 1, 0],
        '<<3>>' => [0, 0, 0, 0, 0, 0, 1, 1],
        '<<4>>' => [0, 0, 0, 0, 0, 1, 0, 0],
        '<<5>>' => [0, 0, 0, 0, 0, 1, 0, 1],
        '<<6>>' => [0, 0, 0, 0, 0, 1, 1, 0],
        '<<7>>' => [0, 0, 0, 0, 0, 1, 1, 1],
    ];

    /**
     * the commands for the eight programmable characters
     */
    const PROGRAMMABLE_COMMANDS       = [
        '0' => [0, 1, 0, 0, 0, 0, 0, 0],
        '1' => [0, 1, 0, 0, 1, 0, 0, 0],
        '2' => [0, 1, 0, 1, 0, 0, 0, 0],
        '3' => [0, 1, 0, 1, 1, 0, 0, 0],
        '4' => [0, 1, 1, 0, 0, 0, 0, 0],
        '5' => [0, 1, 1, 0, 1, 0, 0, 0],
        '6' => [0, 1, 1, 1, 0, 0, 0, 0],
        '7' => [0, 1, 1, 1, 1, 0, 0, 0],
    ];

    /**
     * the conversion for numbers to double height
     */
    const DOUBLE_HEIGHT_NUMBER_LOOKUP = [
        '0' => [
            'line1' => '<<1>>',
            'line2' => '<<3>>',
        ],
        '1' => [
            'line1' => '<<0>>',
            'line2' => '<<0>>',
        ],
        '2' => [
            'line1' => '<<7>>',
            'line2' => '<<4>>',
        ],
        '3' => [
            'line1' => '<<7>>',
            'line2' => '<<2>>',
        ],
        '4' => [
            'line1' => '<<3>>',
            'line2' => '<<0>>',
        ],
        '5' => [
            'line1' => '<<4>>',
            'line2' => '<<6>>',
        ],
        '6' => [
            'line1' => '<<4>>',
            'line2' => '<<3>>',
        ],
        '7' => [
            'line1' => '<<7>>',
            'line2' => '<<0>>',
        ],
        '8' => [
            'line1' => '<<1>>',
            'line2' => '<<5>>',
        ],
        '9' => [
            'line1' => '<<1>>',
            'line2' => '<<2>>',
        ],
        ':' => [
            'line1' => ':',
            'line2' => ':',
        ],
    ];

    /**
     * @var array
     */
    private $programmableChars;

    /**
     * Private constructor to prevent instantiation.
     *
     * @param string|null $programmableChars location of ini file with definitions of programmable chars
     */
    public function __construct($programmableChars = null)
    {
        if (!is_null($programmableChars) && is_file($programmableChars)) {
            $this->programmableChars = require $programmableChars;
        } else {
            $this->programmableChars = [
                '0' => [
                    '1' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '2' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 0, 0, 0, 0, 1],
                ],
                '1' => [
                    '1' => [0, 0, 0, 1, 1, 1, 1, 1],
                    '2' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 1, 0, 0, 0, 1],
                ],
                '2' => [
                    '1' => [0, 0, 0, 1, 1, 1, 1, 1],
                    '2' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 1, 1, 1, 1, 1],
                ],
                '3' => [
                    '1' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '2' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 1, 1, 1, 1, 1],
                ],
                '4' => [
                    '1' => [0, 0, 0, 1, 1, 1, 1, 1],
                    '2' => [0, 0, 0, 1, 0, 0, 0, 0],
                    '3' => [0, 0, 0, 1, 0, 0, 0, 0],
                    '4' => [0, 0, 0, 1, 0, 0, 0, 0],
                    '5' => [0, 0, 0, 1, 0, 0, 0, 0],
                    '6' => [0, 0, 0, 1, 0, 0, 0, 0],
                    '7' => [0, 0, 0, 1, 0, 0, 0, 0],
                    '8' => [0, 0, 0, 1, 1, 1, 1, 1],
                ],
                '5' => [
                    '1' => [0, 0, 0, 1, 1, 1, 1, 1],
                    '2' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 1, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 1, 1, 1, 1, 1],
                ],
                '6' => [
                    '1' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '2' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 1, 1, 1, 1, 1],
                ],
                '7' => [
                    '1' => [0, 0, 0, 1, 1, 1, 1, 1],
                    '2' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '3' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '4' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '5' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '6' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '7' => [0, 0, 0, 0, 0, 0, 0, 1],
                    '8' => [0, 0, 0, 0, 0, 0, 0, 1],
                ],
            ];
        }
    }

    /**
     * @return array
     */
    public function getProgrammableChars()
    {
        return $this->programmableChars;
    }

}
