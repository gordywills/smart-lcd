<?php

namespace LCD\Instruction;

use LCD\Driver\Connection;

/**
 * Class LCD\Instruction\Command
 * A class to control sending command instcutions to the LCD display
 *
 * @package LCD
 */
class Command extends Instruction
{
    /**#@+
     * command options
     *
     * @var int
     */
    const FOUR_BIT_DATA_LENGTH  = 0;
    const EIGHT_BIT_DATA_LENGTH = 1;
    const ONE_LINE_DISPLAY      = 0;
    const TWO_LINE_DISPLAY      = 1;
    const FIVE_BY_EIGHT_FONT    = 0;
    const FIVE_BY_TEN_FONT      = 1;
    const CURSOR_DECREMENT      = 0;
    const CURSOR_INCREMENT      = 1;
    const ENTRY_MODE_NO_SHIFT   = 0;
    const ENTRY_MODE_SHIFT      = 1;
    const DISPLAY_OFF           = 0;
    const DISPLAY_ON            = 1;
    const CURSOR_OFF            = 0;
    const CURSOR_ON             = 1;
    const BLINK_OFF             = 0;
    const BLINK_ON              = 1;
    const SHIFT_CURSOR          = 0;
    const SHIFT_DISPLAY         = 1;
    const SHIFT_LEFT            = 0;
    const SHIFT_RIGHT           = 1;
    const START_OF_SECOND_LINE  = [1, 0, 0, 0, 0, 0, 0];

    /**
     * Clears display and returns cursor to the home position (address 0).
     */
    public function clearDisplay()
    {
        $pattern = [0, 0, 0, 0, 0, 0, 0, 1];
        $this->sendCommand($pattern);
    }

    /**
     * A wrapper to send instructions to the data register
     *
     * @param array $pattern the 8bit command to be sent (see datasheet for more (LCD\Resources\HD44780.pdf))
     */
    private function sendCommand($pattern)
    {
        $this->sendInstruction($pattern, Connection::COMMAND_REGISTER);
    }

    /**
     * Returns cursor to home position. Also returns display being shifted to the original position. DDRAM content
     * remains unchanged.
     */
    public function cursorHome()
    {
        $pattern = [0, 0, 0, 0, 0, 0, 1, 0];
        $this->sendCommand($pattern);
    }

    /**
     * Sets cursor move direction; specifies whether to shift the display. These operations are performed during data
     * read/write.
     *
     * @param int $cursorDirection increment or decrement
     * @param int $displayShift    shift or no_shift (shift mode make the cursor home position the centre of the DDRAM)
     */
    public function entryModeSet(
        $cursorDirection = self::CURSOR_INCREMENT,
        $displayShift = self::ENTRY_MODE_NO_SHIFT
    ) {
        $pattern = [0, 0, 0, 0, 0, 1, $cursorDirection, $displayShift];
        $this->sendCommand($pattern);
    }

    /**
     * Sets on/off of all display, cursor on/off, and blink of cursor position character (if cursor on).
     *
     * @param int $onOff       switch the whole display on or off (when off DDRAM can still be written to so that when
     *                         switched back on a new message is displayed)
     * @param int $cursorOnOff show the cursor in the DDRAM (cursor is _ character)
     * @param int $blink       flash the cursor (full block)
     */
    public function displayOnOff(
        $onOff = self::DISPLAY_ON,
        $cursorOnOff = self::CURSOR_OFF,
        $blink = self::BLINK_OFF
    ) {
        $pattern = [0, 0, 0, 0, 1, $onOff, $cursorOnOff, $blink];
        $this->sendCommand($pattern);
    }

    /**
     * Either moves the cursor by a single character OR shifts the DDRAM under the window. DDRAM content remains
     * unchanged.
     *
     * @param int $cursorOrDisplay which item to shift
     * @param int $direction       direction of shift left or right
     */
    public function cursorDisplayShift($cursorOrDisplay = self::SHIFT_DISPLAY, $direction = self::SHIFT_LEFT)
    {
        $pattern = [0, 0, 0, 1, $cursorOrDisplay, $direction, 0, 0];
        $this->sendCommand($pattern);
    }

    /**
     * Sets interface data length, number of display line, and character font.  Is used for initial set up.
     *
     * @param int $dataLength    4 bit or 8 bit
     * @param int $numberOfLines one or two line duty (see datasheet for more (LCD\Resources\HD44780.pdf) - setting
     *                           this incorrectly will produce unexpected results)
     * @param int $font          font is either 8 or ten pixels high match to your physical display
     */
    public function functionSet(
        $dataLength = self::EIGHT_BIT_DATA_LENGTH,
        $numberOfLines = self::TWO_LINE_DISPLAY,
        $font = self::FIVE_BY_EIGHT_FONT
    ) {
        $pattern = [0, 0, 1, $dataLength, $numberOfLines, $font, 0, 0];
        $this->sendCommand($pattern);
    }

    /**
     * Sets the DDRAM address shown at the top left of the window.
     *
     * @param array $address a 7 bit address as defined in the datasheet (LCD\Resources\HD44780.pdf).
     */
    public function scrollToAddress(array $address = self::START_OF_SECOND_LINE)
    {
        $pattern = array_merge([1], $address);
        $this->sendCommand($pattern);
    }

    /**
     * each time the LCD is switched off the programmable chars are lost and must be reinitialised on power up.  This
     * method reads the programmable chars from the character codes class and codes them into the register
     */
    public function initialiseProgrammableChars()
    {
        foreach ($this->getCharacters() as $char => $pattern) {
            $this->programmeChar(CharacterCodes::PROGRAMMABLE_COMMANDS[$char], $pattern);
        }
    }

    /**
     * This method codes in a single character of 8 rows represented by $pattern and addeseed by the command function
     * $address
     *
     * @param array $address an array of 8 binary digits representing the command start address of one of the
     *                       programmable characters
     * @param array $pattern an array of 8 lines, each line represetning an 8 digit binary code for each line of the
     *                       programmable character where the MS 3 bits are always 0 and the LSB 5 bits are 1 for dark
     *                       and 0 for light
     */
    private function programmeChar($address, $pattern)
    {
        $this->sendCommand($address);
        foreach ($pattern as $line) {
            $this->sendInstruction($line, Connection::DATA_REGISTER);
        }
    }
}