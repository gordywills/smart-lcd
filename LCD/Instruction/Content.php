<?php

namespace LCD\Instruction;

use LCD\Driver\Connection;

/**
 * Class LCD\Instruction\Content
 * A class to control the sending of content to the LCD display
 *
 * @package LCD
 */
class Content extends Instruction
{
    /**
     * print a string at the cursor
     *
     * @param string $content the string to print
     */
    public function printContentAtCursor($content)
    {
        $chars = $this->splitStringIntoChars($content);
        foreach ($chars as $char) {
            $this->printChar($char);
        }
    }

    /**
     * split a string in to characters for printing and handle special chars from programmable range with pattern
     * /<<\d>>/
     *
     * @param string $string the string to split
     *
     * @return array
     */
    private function splitStringIntoChars($string)
    {
        $pattern = '/<<\d>>/';
        $matches = [];
        if (!preg_match_all($pattern, $string, $matches)) {
            return str_split($string);
        }
        $result = [];
        $parts = preg_split($pattern, $string);

        foreach ($parts as $key => $part) {
            if(strlen($part)>0){
                $result = array_merge($result, str_split($part));
            }
            if (isset($matches[0][$key])){
                $result[] = $matches[0][$key];
            }
        }

        return $result;
    }

    /**
     * Count the number of displayed chars from a string even if it has programmable chars in it
     *
     * @param string $string the string to be counted (progammable chars in format /<<\d>>/ are couted as one char)
     *
     * @return int
     */
    public function countStringLength($string)
    {
        return count($this->splitStringIntoChars($string));
    }

    /**
     * Print a character at the cursor
     *
     * @param string $char a single character string to be printed (unrecognised chars are rendered as spaces)
     */
    private function printChar($char)
    {
        $charPattern = $this->lookupCharacterPattern($char);
        $this->sendContent($charPattern);
    }

    /**
     * Look up the pattern for a given character unrecognised chars return the pattern for a space
     *
     * @param string $char a single character string to look up the pattern for
     *
     * @return array
     */
    private function lookupCharacterPattern($char)
    {
        $patterns = CharacterCodes::CHARACTERS;

        return isset($patterns[$char]) ? $patterns[$char] : $patterns[' '];
    }

    /**
     * A wrapper to send instructions to the data register
     *
     * @param array $pattern the 8bit data message to be sent
     */
    private function sendContent($pattern)
    {
        $this->sendInstruction($pattern, Connection::DATA_REGISTER);
    }
}