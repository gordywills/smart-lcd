<?php

namespace LCD\Instruction;

use LCD\Driver\Connection;

/**
 * Class LCD\Instruction\Instruction
 * an abstract class for the instruction classes
 *
 * @package LCD
 */
abstract class Instruction
{
    /**
     * Physical width in characters of the display (sitting above the DDRAM space)
     */
    const WINDOW_WIDTH = 16;
    /**
     * The width of the DDRAM which can be scrolled back and forth
     */
    const DISPLAY_WIDTH = 40;

    /**
     * @var Connection
     */
    private $driver;
    /**
     * @var CharacterCodes
     */
    private $characters;

    /**
     * LCDAbstract constructor.
     *
     * @param Connection          $driver
     * @param CharacterCodes|null $characters
     */
    public function __construct(Connection $driver, CharacterCodes $characters = null)
    {
        $this->driver = $driver;
        $this->characters = $characters;
    }

    /**
     * Build and instruction on the LCD driver register and then call transact
     *
     * @param array $pattern  An 8 bit array of the instruction to be sent in human readable binary order
     * @param int   $register which register (DDRAM or Command) use class constants
     */
    protected function sendInstruction($pattern, $register)
    {
        $this->driver->sendInstruction($pattern, $register);
    }

    /**
     * Get the programmable character list
     *
     * @return array
     */
    protected function getCharacters()
    {
        return $this->characters->getProgrammableChars();
    }
}