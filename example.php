#!/usr/bin/env php
<?php

//initiate the composer autoload
require 'vendor/autoload.php';

//Dependencies
use LCD\WorkerFactory;

$LCD = WorkerFactory::create();
$firstLine = 'This is a 16x2';
$secondLine = 'LCD Display';
$firstLongLine = 'This message is very long.';
$secondLongLine = 'It causes the display to scroll.';
while (true) {
    $LCD->displayTime(new DateTime());
    sleep(15);
    $LCD->displayMessages($firstLine,$secondLine);
    sleep (15);
    $LCD->displayTime(new DateTime());
    sleep(15);
    $LCD->displayMessages($firstLongLine,$secondLongLine);
    sleep (15);
}